<?php

define('BASE_URI', "http://{$_SERVER['SERVER_NAME']}" );
/**
 * Configuracao de Diretorios
 */
define('APP', 'app' . DIRECTORY_SEPARATOR );
define('BASEURL', APP );
define('CLASSDIR', APP . 'class' . DIRECTORY_SEPARATOR );
define("VIEWSDIR", APP . "views" . DIRECTORY_SEPARATOR );
define("HELPERDIR", APP . "helpers" . DIRECTORY_SEPARATOR );
define("MODELDIR", APP . "models" . DIRECTORY_SEPARATOR );
define("DATABASEDIR", APP . "database" . DIRECTORY_SEPARATOR );
define("REALPATH", dirname(dirname( __FILE__ )));
define("REALPATH_APP", dirname(dirname( __FILE__ )) . DIRECTORY_SEPARATOR.APP );
define("REALPATH_VIEWS", dirname(dirname( __FILE__ )) . DIRECTORY_SEPARATOR . APP . "views" );

/**
 * BANCO DE DADOS
 */
define('DB_HOST', 'localhost');
define('DB_NAME', 'vistasoft_marcelo');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_PORT', '');
define('DB_DRIVER', 'mysql');

/**
 * API VistaSoft
 */
define('API_VS_KEY', 'c9fdd79584fb8d369a6a579af1a8f681&showtotal');
define('API_VS_URL', 'http://sandbox-rest.vistahost.com.br/imoveis/listar?key='.API_VS_KEY);

define('UTF8_ENABLED', FALSE);
