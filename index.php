<?php
//error_reporting(0);
function get_current_url()
{
    $protocol = 'http';
    if ( $_SERVER['SERVER_PORT'] == 443 || (!empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on') )
    {
        $protocol .= 's';
        $protocol_port = $_SERVER['SERVER_PORT'];
    }
    else
    {
        $protocol_port = 80;
    }
    $host = $_SERVER['HTTP_HOST'];
    $port = $_SERVER['SERVER_PORT'];
    $request = $_SERVER['PHP_SELF'];
    if ( isset( $_SERVER['argv'][0] ) )
        $query = substr( $_SERVER['argv'][0], strpos( $_SERVER['argv'][0], ';' ) + 1 );
    $toret = $protocol . '://' . $host . ($port == $protocol_port ? '' : ':' . $port) . $request . (empty( $query ) ? '' : '?' . $query);
    return $toret;
}

if ( !preg_match( '/www/', get_current_url() ) )
{
    $protocol = "http://";
}
else
{
    $protocol = 'http://www.';
}
$url = explode( "index.php", get_current_url() );
$httpurl = $url[0];
$sub = explode( ".", $httpurl );
if ( count( $sub ) >= 4 )
{
    $sub = preg_replace( '/http:\/\//', '', $sub[0] );
    define( 'SUB', "$sub" );
}
else
{
    define( 'SUB', "" );
}
# Configurar diretorio do projeto HTTPURL
define( "HTTPURL", "$httpurl" );

# Subdirs e base configurados em .htaccess
# nao alterar
if ( isset( $_GET['dir'] ) && $_GET['dir'] != '' )
{
    if ( substr( $_GET['dir'], -1 ) != '/' )
    {
        $dirname = APP . $_GET['dir'] . "/";
    }
    else
    {
        $dirname = APP . $_GET['dir'];
    }
    if ( is_dir( $dirname ) )
    {
        define( 'CTRL', "$dirname" );
    }
    else
    {
        @header( 'Location: erro404.php' );
    }
}
else
{
    define( 'CTRL', APP );
}

# Route .htaccess
if ( isset( $_GET['route'] ) )
{
    $routes = explode( "/", $_GET['route'] );
    if ( count( $routes ) == 1 )
    {
        $routes[1] = "error";
    }
    $class = ucfirst($routes[0]);
    if ( isset( $routes[1] ) && $routes[0] != SUB )
    {
        $p1 = isset($routes[2]) ? $routes[2] : '';
        $action = $routes[1];
        $obj = new $class;
        ( method_exists( $obj, $action ) ) ? $obj->$action($p1) : $obj->index();
    }
}

function __autoload( $class )
{
    require_once CLASSDIR . "VSM.php";
    require_once CLASSDIR . "Database.php";

    $class_file = CLASSDIR . ucfirst( $class ) . '.php';
    $ctrl_file = CTRL . ucfirst( $class ) . '.php';

    if ( file_exists( $class_file ) )
    {
        include_once $class_file;
    }
    elseif ( file_exists( $ctrl_file ) )
    {
        include_once $ctrl_file;
    }
    elseif ( file_exists( ucfirst( $ctrl_file ) ) )
    {
        include_once ucfirst( $ctrl_file );
    }
    else
    {
        @header( 'Location:' . HTTPURL . 'erro404.php' );
    }
}