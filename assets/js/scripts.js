$(document).ready(function(){
    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };
    $(".telefone").mask(SPMaskBehavior, spOptions);
    $(".valor").mask('#.##0,00', {reverse: true});

    $('#imovel_cod_vista').on('change', function (){
        $('#imovel_endereco').val($("#imovel_cod_vista option:selected").html());
    });
})