<?php

class Index extends VSM
{
    public function __construct()
    {
        $this->helper('util');
        $this->model(array('imovel'));
    }

    public function index()
    {
        include_once "Imovel.php";

        $imovel = New Imovel();
        $imovel->listar();
    }


}