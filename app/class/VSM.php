<?php
session_start();

class VSM
{
    public $data = [];

    function __construct()
    {
    }

    function template($arquivo = null, $dados=null)
    {
        require_once REALPATH_VIEWS . DIRECTORY_SEPARATOR . "_inc" . DIRECTORY_SEPARATOR . "cabecalho.php";
        if ($arquivo != null) {
            if($dados != null){
                if(!is_array($dados)){
                    die("Erro no template: parametro dados deve ser um array. Verifique!");
                }
            }
//            foreach ($dados as $k => $v) {
//                if(is_array($dados[$k])){
//
//                    $k = $dados[$k];
//                    $clientes = $dados[$k];
//
//                }
//            }
//            $clientes = $dados['clientes'];
            if (!file_exists(REALPATH_VIEWS . DIRECTORY_SEPARATOR . "{$arquivo}.php")) {
                die("Template <b>{$arquivo}.php</b> nao localizado, verifique!");
            }
            require_once REALPATH_VIEWS . DIRECTORY_SEPARATOR . "{$arquivo}.php";
        } else {
            die("Template nao informado, verifique!");
        }
        require_once REALPATH_VIEWS . DIRECTORY_SEPARATOR . "_inc" . DIRECTORY_SEPARATOR . "rodape.php";
    }

    function helper($arq = null)
    {
        if (is_array($arq)) {
            foreach ($arq as $a) {
                require_once HELPERDIR .  "{$a}_helper.php";
            }
        } else {
            require_once HELPERDIR . "{$arq}_helper.php";
        }

    }

    function model($arq = null)
    {
        if (is_array($arq)) {
            foreach ($arq as $a) {
                $a_model = "{$a}_model";
                require_once MODELDIR . "{$a_model}.php";
                $am = "{$a}Model";
                $this->$a_model = new $am;
            }
        } else {
            $arq_model = "{$arq}_model";
            require_once MODELDIR . "{$arq_model}.php";
            $am = "{$arq}Model";
            $this->$arq_model = new $am;
        }
    }

    public function imoveis_api()
    {
        $dados = array(
            'fields' => array(
                "Codigo",
                "Categoria",
                "Bairro",
                "Cidade",
                "ValorVenda",
                "ValorLocacao",
                "Dormitorios",
                "Suites",
                "Vagas",
                "AreaTotal",
                "AreaPrivativa",
                "Caracteristicas",
                "InfraEstrutura",
//        "Foto"=>["Foto","FotoPequena","Destaque"]
            )
        );

        $postFields = json_encode($dados);
        $url = API_VS_URL ."&pesquisa={$postFields}";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        return curl_exec($ch);
    }

    public function log($txt)
    {
        $data = date("d/m/Y H:i:s") . " | ";
        $arquivo = fopen('log/log_'.date('Ymd').'.txt','wr+');
        if ($arquivo == false) die('Não foi possível criar o log.');
        fwrite("log/{$arquivo}", "{$data}{$txt}");
        fclose("log/{$arquivo}");
    }
}
