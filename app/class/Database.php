<?php

/**
 * Class Database
 *
 * CRUD generico
 * @author Marcelo Mendes Nazario
 */

class Database
{
    protected static $dbase;
    var $table = "";

    public function __construct()
    {

        $db_host    = DB_HOST;
        $db_nome    = DB_NAME;
        $db_user    = DB_USER;
        $db_pass    = DB_PASS;
        $db_driver  = DB_DRIVER;

        try
        {
            self::$dbase = new PDO("{$db_driver}:host={$db_host}; dbname={$db_nome}", $db_user, $db_pass);
            self::$dbase->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$dbase->exec('SET NAMES utf8');
        }
        catch (PDOException $e)
        {
            $this->log("PDOException em " . $e->getMessage());
            die("Connection Error: " . $e->getMessage());
        }
    }

    /**
     * @return PDO
     *
     * Conecta com o banco de dados
     */
    public static function conexao()
    {
        if (!self::$dbase)
        {
            new Database();
        }
        return self::$dbase;
    }

    /**
     * @param array $dados
     * @return false|string
     *
     * Insere registro no banco de dados
     */
    public function insert($dados=[]){
        if($this->table == null){
            die("Tabela nao foi informada");
        }

        if(count($dados) > 0){

            $columns = implode(", ", array_keys($dados));
            $values = ":" . implode(", :", array_keys($dados));
            $pdo = $this->conexao();
            $insert = $pdo->prepare("INSERT INTO {$this->table} ({$columns}) VALUES ({$values})");
            $insert->execute($this->filter($dados));
            if ($insert->rowCount() > 0) {
                return $pdo->lastInsertId();;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * @param array $dados
     * @param array $where
     * @return bool
     *
     * Altera registro no banco de dados
     */
    public function update($dados=[], $where=[]){
        if($this->table == null){
            die("Tabela nao foi informada");
        }

        $data_set = [];
        if(count($dados) > 0) {
            foreach ($dados as $k => $v) {
                $data_set[] = "{$k} = :{$k}";
            }
            $data_set = implode(", ", $data_set);

            // só executo se for informado alguma condição no WHERE
            if (count($where) > 0) {

                $a_where = '';
                foreach ($where as $k => $v) {
                    $a_where[] = "{$k} = '{$v}'";
                }
                $str_where = implode(' AND ', $a_where);

                $pdo = $this->conexao();
                $update = $pdo->prepare("UPDATE {$this->table} SET {$data_set} WHERE {$str_where}");
                $update->execute($this->filter($dados));
                if ($update->rowCount() > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                die("Informe no mímino uma condição no parametro WHERE para fazer update.");
            }
        }
    }

    /**
     * @return array|null
     *
     * Retornas todos os dados da tabela no banco de dados
     */
    public function all(){
        if($this->table == null){
            die("Tabela nao foi informada");
        }

        $pdo = $this->conexao();
        $select = $pdo->prepare("SELECT * FROM {$this->table}");
        if($select->execute()){
            return $select->fetchAll(PDO::FETCH_OBJ);
        }else{
            return null;
        }
    }

    /**
     * @param array $where
     * @return array|null
     *
     * Retorna dados da tabela no banco de dados de acordo com o $where
     */
    public function get($where=[]){
        if($this->table == null){
            die("Tabela nao foi informada");
        }

        if(count($where) > 0) {

            $arr_where = '';
            foreach ($where as $k => $v) {
                $arr_where[] = "{$k} = '{$v}'";
            }
            $str_where = implode(' AND ', $arr_where);
            $pdo = $this->conexao();
            $select = $pdo->prepare("SELECT * FROM {$this->table} WHERE {$str_where}");
            if ($select->execute()) {
                return $select->fetchAll(PDO::FETCH_OBJ);
            } else {
                return null;
            }
        }
    }


    /**
     * @param array $where
     * @return mixed|null
     *
     * Retorna dadosde um registro da tabela acordo com o $where
     */
    public function get_by_campo($where=[]){
        if($this->table == null){
            die("Tabela nao foi informada");
        }

        if(count($where) > 0) {

            $arr_where = '';
            foreach ($where as $k => $v) {
                $arr_where[] = "{$k} = '{$v}'";
            }
            $str_where = implode(' AND ', $arr_where);
            $pdo = $this->conexao();
            $select = $pdo->prepare("SELECT * FROM {$this->table} WHERE {$str_where}");
            if ($select->execute()) {
                return $select->fetch(PDO::FETCH_ASSOC);
            } else {
                return null;
            }
        }
    }

    /**
     * @param $where
     * @return bool
     *
     * Deleta registro da tabela
     */
    public function delete($where){
        if($this->table == null){
            die("Tabela nao foi informada");
        }

        $arr_where = '';
        foreach ($where as $k => $v) {
            $arr_where[] = "{$k} = {$v}";
        }
        $str_where = implode(' AND ', $arr_where);

        $pdo = $this->conexao();
        $delete = $pdo->prepare("DELETE FROM {$this->table} WHERE {$str_where}");
        if ($delete->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $campo
     * @param $valor
     * @return bool
     *
     * Verifica se a informação já existe no banco de dados
     */
    public function existe($where=[])
    {
        $arr_where = '';
        foreach ($where as $k => $v) {
            if(is_numeric($v)){
                $arr_where[] = "{$k} = {$v}";
            }else{
                $arr_where[] = "{$k} = '{$v}'";
            }
        }
        $str_where = implode(' AND ', $arr_where);
        $pdo = $this->conexao();
        $stmt = $pdo->prepare("SELECT * FROM {$this->table} WHERE {$str_where}");
        $stmt->execute();

        if ($stmt->rowcount() <= 0) {
            return false;
        }

        return true;
    }

    /**
     * @param array $data
     * @return array
     *
     * Filtra o value do array para verificar se é nulo, e remove caracteres especiais
     */
    private function filter(array $data)
    {
        $filter = [];
        foreach ($data as $key => $value) {
            $filter[$key] = (is_null($value) ? null : filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS));
        }
        return $filter;
    }
}