<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>VistaSoft Marcelo</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        body {
            font-family: "Lato", sans-serif;
            overflow-x: hidden;
            -ms-overflow-x: hidden;

        }

        .sidenav {
            height: 100%;
            width: 160px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #111;
            overflow-x: hidden;
            padding-top: 20px;
        }

        .sidenav a {
            padding: 6px 8px 6px 16px;
            text-decoration: none;
            font-size: 16px;
            color: #818181;
            display: block;
        }

        .sidenav a:hover {
            color: #f1f1f1;
        }

        .sidenav h4{
            color: #FFF;
            text-align: center;
            border-bottom: 1px solid #363433 !important;
            padding-bottom: 21px !important;
        }

        .sidenav h4 span{
            font-size: 10px;
            display: block;
        }

        .main {
            margin-left: 160px; /* Same as the width of the sidenav */
            font-size: 15px; /* Increased text to enable scrolling */
            padding: 30px 10px;
        }

        h4{
            /*border-bottom: 1px double #cccccc;*/
            margin-bottom: 20px;
        }

        @media screen and (max-height: 450px) {
            .sidenav {padding-top: 15px;}
            .sidenav a {font-size: 18px;}
        }
    </style>
</head>
<body>

<div class="sidenav">
    <h4>
        VSM
        <span>VistaSoft Marcelo</span>
    </h4>
    <a href="<?=url('imovel/listar')?>">Imóveis</a>
    <a href="<?=url('cliente/listar')?>">Clientes</a>
    <a href="<?=url('proprietario/listar')?>">Proprietários</a>
    <a href="<?=url('contrato/listar')?>">Contratos</a>
    <a href="<?=url('financeiro/listar')?>">Financeiro</a>
    <a href="<?=url('api/listar')?>">Listar imóveis API</a>

</div>

<div class="main">
    <?=get_msg_flash();?>

