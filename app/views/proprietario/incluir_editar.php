
<div class="row">
    <div class="col-md-12">
        <a href="<?=url('proprietario/listar');?>" class="btn btn-sm btn-secondary">Listar</a>
        <hr />
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h4><?=$this->data['acao']?> proprietário</h4>
        <form action="<?=url('proprietario/salvar')?>" method="post">
            <input type="hidden" name="proprietario_id" value="<?=$this->data['proprietario_id']?>">
            <div class="form-group">
                <label for="proprietario_nome">Nome:</label>
                <input type="text" class="form-control" name="proprietario_nome" id="proprietario_nome" value="<?=$this->data['proprietario_nome']?>" required>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="proprietario_email">E-mail:</label>
                        <input type="email" class="form-control" name="proprietario_email" id="proprietario_email" value="<?=$this->data['proprietario_email']?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="proprietario_telefone">Telefone:</label>
                        <input type="text" class="form-control telefone" name="proprietario_telefone" id="proprietario_telefone" value="<?=formatar($this->data['proprietario_telefone'], 'telefone')?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="proprietario_data_repasse">Data repasse:</label>
                        <input type="date" class="form-control data" name="proprietario_data_repasse" id="proprietario_data_repasse" value="<?=$this->data['proprietario_data_repasse']?>" required>
                    </div>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-info" name="salvar_cont_ed">Salvar e continuar editando</button>
                <button type="submit" class="btn btn-success" name="salvar">Salvar</button>
            </div>
        </form>
    </div>
</div>



