
<div class="row">
    <div class="col-md-12">
        <a href="<?=url('proprietario/novo');?>" class="btn btn-sm btn-secondary">+ Novo</a>
        <hr />
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4>Proprietários cadastrados</h4>

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col" class="text-right">#</th>
                <th scope="col">Nome</th>
                <th scope="col">E-mail</th>
                <th scope="col">Telefone</th>
                <th scope="col">Data Repasse</th>
                <th scope="col" class="text-center">Ações</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($this->data['proprietarios'] as $k => $v){
                ?>
                <tr>
                    <th scope="row" class="text-right"><?=$this->data['proprietarios'][$k]->proprietario_id?></th>
                    <td><?=$this->data['proprietarios'][$k]->proprietario_nome?></td>
                    <td><?=$this->data['proprietarios'][$k]->proprietario_email?></td>
                    <td><?=$this->data['proprietarios'][$k]->proprietario_telefone?></td>
                    <td><?=$this->data['proprietarios'][$k]->proprietario_data_repasse?></td>
                    <td class="text-center">
                        <a href="<?=url("proprietario/editar/{$this->data['proprietarios'][$k]->proprietario_id}");?>" class="btn btn-sm btn-info">Editar</a>
                        <a href="<?=url("proprietario/excluir/{$this->data['proprietarios'][$k]->proprietario_id}");?>" class="btn btn-sm btn-danger">Excluir</a>
                    </td>
                </tr>
                <?php
            }
            result_empty($this->data['proprietarios'], ['colspan'=>6])
            ?>
            </tbody>
        </table>
    </div>
</div>

