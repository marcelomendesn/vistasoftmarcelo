
<div class="row">
    <div class="col-md-12">
        <a href="<?=url('cliente/novo');?>" class="btn btn-sm btn-success">+ Novo</a>
        <hr />
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4>Clientes cadastrados</h4>

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col" class="text-right">#</th>
                <th scope="col">Nome</th>
                <th scope="col">E-mail</th>
                <th scope="col">Telefone</th>
                <th scope="col" class="text-center">Ações</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($this->data['clientes'] as $k => $v){
                ?>
                <tr>
                    <th scope="row" class="text-right"><?=$this->data['clientes'][$k]->cliente_id?></th>
                    <td><?=$this->data['clientes'][$k]->cliente_nome?></td>
                    <td><?=$this->data['clientes'][$k]->cliente_email?></td>
                    <td><?=$this->data['clientes'][$k]->cliente_telefone?></td>
                    <td class="text-center">
                        <a href="<?=url("cliente/editar/{$this->data['clientes'][$k]->cliente_id}");?>" class="btn btn-sm btn-info">Editar</a>
                        <a href="<?=url("cliente/excluir/{$this->data['clientes'][$k]->cliente_id}");?>" class="btn btn-sm btn-danger">Excluir</a>
                        <a href="<?=url("financeiro/listar/{$this->data['clientes'][$k]->cliente_id}");?>" class="btn btn-sm btn-success">Financeiro</a>
                    </td>
                </tr>
                <?php
            }
            result_empty($this->data['clientes'], ['colspan'=>5])
            ?>
            </tbody>
        </table>
    </div>
</div>

