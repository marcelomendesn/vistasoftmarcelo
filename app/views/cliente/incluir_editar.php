
<div class="row">
    <div class="col-md-12">
        <a href="<?=url('cliente/listar');?>" class="btn btn-sm btn-secondary">Listar</a>
        <hr />
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h4><?=$this->data['acao']?> cliente</h4>
        <form action="<?=url('cliente/salvar')?>" method="post">
            <input type="hidden" name="cliente_id" value="<?=$this->data['cliente_id']?>">
            <div class="form-group">
                <label for="cliente_nome">Nome:</label>
                <input type="text" class="form-control" name="cliente_nome" id="cliente_nome" value="<?=$this->data['cliente_nome']?>" required>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="cliente_email">E-mail:</label>
                        <input type="email" class="form-control" name="cliente_email" id="cliente_email" value="<?=$this->data['cliente_email']?>" required>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="cliente_telefone">Telefone:</label>
                        <input type="text" class="form-control telefone" name="cliente_telefone" id="cliente_telefone" value="<?=formatar($this->data['cliente_telefone'], 'telefone')?>" required>
                    </div>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-info" name="salvar_cont_ed">Salvar e continuar editando</button>
                <button type="submit" class="btn btn-success" name="salvar">Salvar</button>
            </div>
        </form>
    </div>
</div>



