
<div class="row">
    <div class="col-md-12">
        <a href="<?=url('imovel/novo');?>" class="btn btn-sm btn-secondary">+ Novo</a>
        <hr />
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4>Imóveis cadastrados</h4>

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col" class="text-right">#</th>
                <th scope="col">Cód. VistaSoft</th>
                <th scope="col">Categoria</th>
                <th scope="col">Bairro</th>
                <th scope="col">Cidade</th>
                <th scope="col">Proprietário</th>
                <th scope="col" class="text-center">Ações</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($this->data['imoveis'] as $k => $v){
                ?>
                <tr>
                    <th scope="row" class="text-right"><?=$this->data['imoveis'][$k]->imovel_id?></th>
                    <td><?=$this->data['imoveis'][$k]->imovel_cod_vista?></td>
                    <td><?=$this->data['imoveis'][$k]->imovel_categoria?></td>
                    <td><?=$this->data['imoveis'][$k]->imovel_bairro?></td>
                    <td><?=$this->data['imoveis'][$k]->imovel_cidade?></td>
                    <td><?=$this->data['imoveis'][$k]->proprietario_nome?></td>
                    <td class="text-center">
                        <a href="<?=url("imovel/editar/{$this->data['imoveis'][$k]->imovel_id}");?>" class="btn btn-sm btn-info">Editar</a>
                        <a href="<?=url("imovel/excluir/{$this->data['imoveis'][$k]->imovel_id}");?>" class="btn btn-sm btn-danger">Excluir</a>
                    </td>
                </tr>
                <?php
            }
            result_empty($this->data['imoveis'], ['colspan'=>7])
            ?>
            </tbody>
        </table>
    </div>
</div>