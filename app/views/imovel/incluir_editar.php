
<div class="row">
    <div class="col-md-12">
        <a href="<?=url('imovel/listar');?>" class="btn btn-sm btn-secondary">Listar</a>
        <hr />
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h4><?=$this->data['acao']?> imóvel</h4>
        <form action="<?=url('imovel/salvar')?>" method="post">
            <input type="hidden" name="proprietario_id" value="<?=$this->data['imovel_id']?>">
            <input type="hidden" name="imovel_endereco" id="imovel_endereco" value="<?=$this->data['imovel_endereco']?>">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="imovel_cod_vista">Imóvel (API VistaSoft):</label>
                        <select class="form-control" name="imovel_cod_vista" id="imovel_cod_vista" required>
                            <option value="">Selecione...</option>
                            <?php
                            foreach ($this->data['imoveis'] as $k => $v){

                                $option = "{$this->data['imoveis'][$k]['Codigo']} - ";
                                $option .= "{$this->data['imoveis'][$k]['Categoria']} | ";
                                $option .= "{$this->data['imoveis'][$k]['Bairro']} | ";
                                $option .= "{$this->data['imoveis'][$k]['Cidade']}";

                                $imovel_selected = $this->data['imoveis'][$k]['imovel_selected'];

                                ?>
                                <option value="<?=$this->data['imoveis'][$k]['Codigo']?>" <?=$imovel_selected?> ><?=$option?></option>
                                <?
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="proprietario_id">Proprietário:</label>
                        <select class="form-control" name="proprietario_id" id="proprietario_id" required>
                            <option value="">Selecione...</option>
                            <?php
                            foreach ($this->data['proprietarios'] as $k => $v){
                                $prop_selected = $this->data['proprietarios'][$k]->prop_selected;
                                ?>
                                <option value="<?=$this->data['proprietarios'][$k]->proprietario_id?>" <?=$prop_selected?>><?=$this->data['proprietarios'][$k]->proprietario_nome?></option>
                                <?
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-info" name="salvar_cont_ed">Salvar e continuar editando</button>
                <button type="submit" class="btn btn-success" name="salvar">Salvar</button>
            </div>
        </form>
    </div>
</div>



