<div class="row">
    <div class="col-md-12">
        <a href="<?= url('imovel/listar'); ?>" class="btn btn-sm btn-success">Ir para imóveis cadastrados no sistema</a>
        <hr/>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4>Lista imóveis via API VistaSoft</h4>

        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col" class="text-right">Cod. VistaSoft</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Bairro</th>
                    <th scope="col">Cidade</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($this->data['imoveis'] as $k => $v){
            ?>
                <tr>
                    <th scope="row" class="text-right"><?=$this->data['imoveis'][$k]['Codigo'];?></th>
                    <td><?=$this->data['imoveis'][$k]['Categoria'];?></td>
                    <td><?=$this->data['imoveis'][$k]['Bairro'];?></td>
                    <td><?=$this->data['imoveis'][$k]['Cidade'];?></td>
                </tr>
            <?
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

