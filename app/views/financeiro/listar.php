
<div class="row">
    <div class="col-md-12">
        <a href="<?=url('contrato/novo');?>" class="btn btn-sm btn-success">+ Novo contrato</a>
        <hr />
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4>Financeiro</h4>

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col" class="text-right">#</th>
                <th scope="col">Vencimento</th>
                <th scope="col">Mensalidade</th>
                <th scope="col">Repasse</th>
                <th scope="col">Contrato</th>
                <th scope="col">Cliente</th>
                <th scope="col">Mensalidade paga</th>
                <th scope="col">Repasse feito</th>
                <th scope="col" class="text-center">Ações</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($this->data['financeiro'] as $k => $v){
                $m_paga_selected = $this->data['financeiro'][$k]->financeiro_mensalidade_ok == 1 ? 'selected' : '';
                $r_feito_selected = $this->data['financeiro'][$k]->financeiro_repasse_ok == 1 ? 'selected' : '';
                ?>
                <form action="<?=url('financeiro/salvar')?>" method="post">
                <tr>
                    <th scope="row" class="text-right"><?=$this->data['financeiro'][$k]->financeiro_id?></th>
                    <td><?=formatar($this->data['financeiro'][$k]->financeiro_vencimento, 'data')?></td>
                    <td>R$ <?=moeda($this->data['financeiro'][$k]->financeiro_mensalidade)?></td>
                    <td>R$ <?=moeda($this->data['financeiro'][$k]->financeiro_repasse)?></td>
                    <td><?=$this->data['financeiro'][$k]->contrato_id?></td>
                    <td><?=$this->data['financeiro'][$k]->cliente_nome?></td>
                    <td>
                        <select class="form-control" name="financeiro_mensalidade_ok" id="financeiro_mensalidade_ok">
                            <option value="0">Não</option>
                            <option value="1" <?=$m_paga_selected?>>Sim</option>
                        </select>
                    </td>
                    <td>
                        <select class="form-control" name="financeiro_repasse_ok" id="financeiro_repasse_ok">
                            <option value="0">Não</option>
                            <option value="1" <?=$r_feito_selected?>>Sim</option>
                        </select>
                    </td>
                    <td class="text-center">
                        <input type="hidden" name="financeiro_id" value="<?=$this->data['financeiro'][$k]->financeiro_id?>">
                        <button type="submit" class="btn btn-sm btn-success">Salvar</button>
                        <a href="<?=url("financeiro/excluir/{$this->data['financeiro'][$k]->financeiro_id}");?>" class="btn btn-sm btn-danger">Excluir</a>
                    </td>
                </tr>
                </form>
                <?php
            }
            result_empty($this->data['financeiro'], ['colspan'=>9])
            ?>
            </tbody>
        </table>
    </div>
</div>

