
<div class="row">
    <div class="col-md-12">
        <a href="<?=url('contrato/listar');?>" class="btn btn-sm btn-success">Listar</a>
        <hr />
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h4><?=$this->data['acao']?> contrato</h4>
        <form action="<?=url('contrato/salvar')?>" method="post">
            <input type="hidden" name="contrato_id" value="<?=$this->data['contrato_id']?>">

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="contrato_data_inicio">Data início:</label>
                        <input type="date" class="form-control" name="contrato_data_inicio" id="contrato_data_inicio" value="<?=$this->data['contrato_data_inicio']?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="contrato_data_fim">Data término:</label>
                        <input type="date" class="form-control" name="contrato_data_fim" id="contrato_data_fim" value="<?=$this->data['contrato_data_fim']?>" disabled>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                            <label for="contrato_taxa_admin">Taxa de administração:</label>
                        <input type="text" class="form-control valor" name="contrato_taxa_admin" id="contrato_taxa_admin" value="<?=$this->data['contrato_taxa_admin']?>" required>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="contrato_valor_aluguel">Valor aluguel:</label>
                        <input type="text" class="form-control valor" name="contrato_valor_aluguel" id="contrato_valor_aluguel" value="<?=$this->data['contrato_valor_aluguel']?>" required>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="contrato_valor_condominio">Valor condomínio:</label>
                        <input type="text" class="form-control valor" name="contrato_valor_condominio" id="contrato_valor_condominio" value="<?=$this->data['contrato_valor_condominio']?>" required>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="contrato_valor_iptu">Valor IPTU:</label>
                        <input type="text" class="form-control valor" name="contrato_valor_iptu" id="contrato_valor_iptu" value="<?=$this->data['contrato_valor_iptu']?>" required>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="imovel_id">Imóvel:</label>
                        <select class="form-control" name="imovel_id" id="imovel_id" required>
                            <option value="">Selecione...</option>
                            <?php
                            foreach ($this->data['imoveis'] as $k => $v){
                                $imovel_selected = $this->data['imoveis'][$k]->imovel_selected;
                                ?>
                                <option value="<?=$this->data['imoveis'][$k]->imovel_id?>" <?=$imovel_selected?>><?=$this->data['imoveis'][$k]->imovel_endereco?></option>
                                <?
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="proprietario_id">Proprietário:</label>
                        <select class="form-control" name="proprietario_id" id="proprietario_id" required>
                            <option value="">Selecione...</option>
                            <?php
                            foreach ($this->data['proprietarios'] as $k => $v){
                                $prop_selected = $this->data['proprietarios'][$k]->prop_selected;
                                ?>
                                <option value="<?=$this->data['proprietarios'][$k]->proprietario_id?>" <?=$prop_selected?>><?=$this->data['proprietarios'][$k]->proprietario_nome?></option>
                                <?
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="cliente_id">Cliente:</label>
                        <select class="form-control" name="cliente_id" id="cliente_id" required>
                            <option value="">Selecione...</option>
                            <?php
                            foreach ($this->data['clientes'] as $k => $v){
                                $cli_selected = $this->data['clientes'][$k]->cli_selected;
                                ?>
                                <option value="<?=$this->data['clientes'][$k]->cliente_id?>" <?=$cli_selected?>><?=$this->data['clientes'][$k]->cliente_nome?></option>
                                <?
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-info" name="salvar_cont_ed">Salvar e continuar editando</button>
                <button type="submit" class="btn btn-success" name="salvar">Salvar</button>
            </div>
        </form>
    </div>
</div>



