
<div class="row">
    <div class="col-md-12">
        <a href="<?=url('contrato/novo');?>" class="btn btn-sm btn-success">+ Novo</a>
        <hr />
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4>Contratos cadastrados</h4>

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col" class="text-right">#</th>
                <th scope="col">Datas</th>
                <th scope="col">Valores</th>
                <th scope="col">Imóvel</th>
                <th scope="col">Proprietário / Cliente</th>
                <th scope="col" class="text-center">Ações</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($this->data['contratos'] as $k => $v){
                ?>
                <tr>
                    <th scope="row" class="text-right"><?=$this->data['contratos'][$k]->contrato_id?></th>
                    <td>
                        <b>Data início:</b><br /><?=formatar($this->data['contratos'][$k]->contrato_data_inicio, 'data')?><br />
                        <b>Data término:</b><br /><?=formatar($this->data['contratos'][$k]->contrato_data_fim, 'data')?>
                    </td>
                    <td>
                        <b>Taxa admin:</b> R$ <?=moeda($this->data['contratos'][$k]->contrato_taxa_admin)?><br />
                        <b>Valor aluguel:</b> R$ <?=moeda($this->data['contratos'][$k]->contrato_valor_aluguel)?><br />
                        <b>Valor condomínio:</b> R$ <?=moeda($this->data['contratos'][$k]->contrato_valor_condominio)?><br />
                        <b>Valor IPTU:</b> R$ <?=moeda($this->data['contratos'][$k]->contrato_valor_iptu)?>
                    </td>
                    <td><?=str_replace(' | ', '<br />', $this->data['contratos'][$k]->imovel_endereco)?></td>
                    <td>
                        <b>Proprietário:</b> <?=$this->data['contratos'][$k]->proprietario_nome?><br />
                        <b>Cliente:</b> <?=$this->data['contratos'][$k]->cliente_nome?>
                    </td>
                    <td class="text-center">
                        <a href="<?=url("contrato/editar/{$this->data['contratos'][$k]->contrato_id}");?>" class="btn btn-sm btn-info">Editar</a>
                        <a href="<?=url("contrato/excluir/{$this->data['contratos'][$k]->contrato_id}");?>" class="btn btn-sm btn-danger">Excluir</a>
                        <a href="<?=url("financeiro/listar/{$this->data['contratos'][$k]->cliente_id}");?>" class="btn btn-sm btn-success">Financeiro</a>
                    </td>
                </tr>
                <?php
            }
            result_empty($this->data['contratos'], ['colspan'=>6])
            ?>
            </tbody>
        </table>
    </div>
</div>

