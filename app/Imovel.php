<?php

class Imovel extends VSM
{
    public function __construct()
    {
        // seto os campos como vazio
        $this->data['imovel_id']        = '';
        $this->data['imovel_endereco']  = '';
        $this->data['imovel_cod_vista'] = '';
        $this->data['proprietario_id']  = '';

        $this->helper('util');
        $this->model(array('imovel', 'proprietario'));
    }

    public function index()
    {
        $this->listar();
    }

    public function novo()
    {
        $this->data['acao'] = "Incluir";

        // pegando o imóveis da API VistaSoft
        $imoveis_api = $this->imoveis_api();
        $imoveis = json_decode($imoveis_api, true);
        foreach ($imoveis as $k => $v) {
            $imoveis[$k]['imovel_selected'] = '';
        }
        $this->data['imoveis'] = $imoveis;

        // pegando proprietários
        $get_prop = $this->proprietario_model->all();
        $this->data['proprietarios'] = $get_prop;
        foreach ($get_prop as $k => $v) {
            $get_prop[$k]->prop_selected = '';
        }

        $this->template('imovel/incluir_editar', $this->data);
    }

    public function editar($id = null)
    {
        $this->data['acao'] = "Editar";

        $get = $this->imovel_model->get_by_campo(['imovel_id' => $id]);
        $this->data = array_merge($this->data, $get);

        // pegando o imóveis da API VistaSoft
        $imoveis_api = $this->imoveis_api();
        $imoveis = json_decode($imoveis_api, true);
        foreach ($imoveis as $k => $v) {
            $imoveis[$k]['imovel_selected'] = '';
            if($k == $get['imovel_cod_vista']){
                $imoveis[$k]['imovel_selected'] = 'selected';
            }
        }
        $this->data['imoveis'] = $imoveis;
        $this->data['imovel_endereco'] = $get['imovel_endereco'];

        // pegando proprietários
        $get_prop = $this->proprietario_model->all();
        foreach ($get_prop as $k => $v) {
            $get_prop[$k]->prop_selected = '';
            if($get_prop[$k]->proprietario_id == $get['proprietario_id']){
                $get_prop[$k]->prop_selected = 'selected';
            }
        }
        $this->data['proprietarios'] = $get_prop;

        $this->template('imovel/incluir_editar', $this->data);
    }

    public function listar()
    {
        $get = $this->imovel_model->listar();
        foreach ($get as $k => $v) {
            $sep = explode('-', $get[$k]->imovel_endereco);
            $sep2 = explode('|', $sep[1]);
            $get[$k]->imovel_categoria = trim($sep2[0]);
            $get[$k]->imovel_bairro = trim($sep2[1]);
            $get[$k]->imovel_cidade = trim($sep2[2]);
            $get[$k]->imovel_proprietario = $get[$k]->proprietario_id;

        }
        $this->data['imoveis'] = $get;

        $this->template('imovel/listar', $this->data);
    }

    public function salvar()
    {
        if ($_POST) {
            $dados = [
                'imovel_cod_vista'  => $_POST['imovel_cod_vista'],
                'imovel_endereco'   => $_POST['imovel_endereco'],
                'proprietario_id'   => $_POST['proprietario_id']
            ];

            if (empty($_POST['imovel_id'])) {
                // cadastrar

                $existe = $this->imovel_model->existe(['imovel_cod_vista'=>$_POST['imovel_cod_vista']]);
                if(!$existe){
                    $insert = $this->imovel_model->insert($dados);
                    if (!$insert) {
                        set_msg_flash("Não conseguimos incluir o imóvel!", 'danger');
                    } else {
                        $id = $insert;
                        set_msg_flash("Imóvel inserido com sucesso!", 'success');
                    }
                }else{
                    set_msg_flash("Imóvel de código {$_POST['imovel_cod_vista']} já está cadastrado!", 'danger');
                    redirect('imovel/novo');
                }

            } else {
                // alterar

                $id = $_POST['imovel_id'];

                $get = $this->imovel_model->get_by_campo(['imovel_id'=>$id]);
                if($_POST['imovel_email'] != $get['imovel_email'])
                {
                    $existe = $this->imovel_model->existe(['imovel_email'=>$_POST['imovel_email']]);
                    if($existe){
                        set_msg_flash("Imóvel de código {$_POST['imovel_cod_vista']} já está cadastrado!", 'danger');
                        redirect("imovel/editar/{$id}");
                    }
                }

                $update = $this->imovel_model->update($dados, ['imovel_id'=>$id]);
                if (!$update) {
                    set_msg_flash("Não conseguimos salvar o imóvel!", 'danger');
                } else {
                    set_msg_flash("Imóvel salvo com sucesso!", 'success');
                }
            }
        }

        if (isset($_POST['salvar_cont_ed'])) {
            redirect("imovel/editar/{$id}");
        } elseif (isset($_POST['salvar'])) {
            redirect('imovel/listar');
        } else {
            redirect('imovel/listar');
        }
    }

    public function excluir($id=null)
    {
        $delete = $this->imovel_model->delete(['imovel_id' => $id]);

        if (!$delete) {
            set_msg_flash("Não conseguimos excluir imóvel!", 'danger');
        } else {
            set_msg_flash("Imóvel excluído com sucesso!", 'success');
        }

        redirect('imovel/listar');
    }
}