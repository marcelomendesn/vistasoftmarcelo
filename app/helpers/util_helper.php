<?php
/**
 * @param null $local
 * @return string
 *
 * Monto o link da url, se nao for informado nada no paramentro $local, é retornado o a url base
 */
function url($local = null)
{
    if ($local != null) {
        return BASE_URI . "/{$local}";
    } else {
        return BASE_URI;
    }
}

/**
 * @param null $msg
 * @param string $tipo
 *
 * Informo mensagem flash
 */
function set_msg_flash($msg = null, $tipo = 'info')
{
    $_SESSION['msg_flash'] = $msg;
    $_SESSION['msg_flash_tipo'] = $tipo;
}

/**
 * Pega mensagem flash (mensagem que aparece uma vez, ao atualizar a pagina e mensagem some
 */
function get_msg_flash()
{
    if (isset($_SESSION['msg_flash'])) {
        echo "<div class=\"alert alert-{$_SESSION['msg_flash_tipo']}\" role=\"alert\">";
        echo $_SESSION['msg_flash'];
        echo "</div>";
        unset($_SESSION['msg_flash']);
        unset($_SESSION['msg_flash_tipo']);
    }
}

function redirect($url)
{
    $url = url($url);
    header("Location: {$url}");
}

/**
 * @param $str
 * @param false $lowercase
 * @return string
 */
function limpa_str($str, $lowercase = false)
{
    $str = preg_replace("/[^a-zA-Z0-9 ]/", "", $str);
    if ($lowercase === true) {
        $str = strtolower($str);
    }
    return trim($str);
}

/**
 * @param $n valor
 * @return string
 *
 * Formato o valor em moeda do padrao brasileiro
 */
function moeda($n)
{
    return number_format($n, 2, ',', '.');
}

function limpa_valor($n)
{
    $n = str_replace('.', '', $n);
    $n = str_replace(',', '.', $n);
    return $n;
}

/**
 * @param $qtd_parc
 * @param null $data_inicio_parcela
 * @return array
 *
 * Gerar parcelas a partir da data atual ou informando uma data
 */
function gerar_parcelas($qtd_parc, $data_inicio_parcela = null)
{
    if ($data_inicio_parcela != null) {
        $dia = date("d", strtotime($data_inicio_parcela));
        $mes = date("m", strtotime($data_inicio_parcela));
        $ano = date("Y", strtotime($data_inicio_parcela));
    } else {
        $dia = date("d");
        $mes = date("m");
        $ano = date("Y");
    }

    $datas = [];
    for ($x = 0; $x < $qtd_parc; $x++) {
        $datas[] = date("Y-m-d", strtotime("+" . $x . " month", mktime(0, 0, 0, $mes, $dia, $ano)));
    }

    return $datas;
}

/**
 * @param null $tag
 * @param null $str
 * @param null $extra
 * @return mixed|string|null
 * Monto tag html
 *
 * @example $tag_p = tag('p', 'teste');
 * Saida: <p>teste</p>
 *
 * @example $tag_p = tag('p', 'teste', 'class="minha-classe"');
 * Saida: <p class="minha-classe">teste</p>
 */
function tag($tag = null, $str = null, $extra = null)
{
    $r = $str;
    if ($tag != null) {
        #$r = '<'.$tag.'>'.$str.'</'.$tag.'>';
        $ex = $extra != null ? " {$extra}" : '';
        $r = "<{$tag}{$ex}>{$str}</{$tag}>";
    }

    return $r;
}

/**
 * @param int $result
 * @param array $dados
 *
 * Monta o <td> quando nao retornar valor no result de um select
 */
function result_empty($result = -1, $dados = [])
{
    if ($result == null || $result == 0) {
        $colspan = 1;
        $texto = 'Nenhuma resultado encontrado.';
        if (key_exists('colspan', $dados)) {
            $colspan = !empty($dados['colspan']) ? $dados['colspan'] : $colspan;
        }
        if (key_exists('texto', $dados)) {
            $texto = !empty($dados['texto']) ? $dados['texto'] : $texto;
        }
        echo tag('td', $texto, 'colspan="' . $colspan . '"');
    }
}

/**
 * Tem por objetivo formartar uma string
 * @param $str : string a ser formatada
 * @param $formatacao : tipo de formatação que deve ser retornada
 * @param $personalizado : se for TRUE o $tipo deverá ser da forma que deseja, porém utilizando o caracter sustenido -> #
 * @author: Marcelo Mendes Nazario
 *
 * @example: Pré-definido: formatar('00000000000', 'cpf'); // retorno: 000.000.000-00
 * @example: Personalizado: formatar('00000000000', '#-#-#-#-#-#-#-#-#-#-#', TRUE); // retorno: 0-0-0-0-0-0-0-0-0-0-0
 *
 */
function formatar($str = null, $formatacao = null, $personalizado = false)
{
    if ($str != null && $formatacao != null) {

        // tipos
        $cnpj = '##.###.###/####-##';
        $cpf = "###.###.###-##";

        if ($formatacao == 'cpf') {
            $count = strlen($str);
            $digitos_cpf = 11;
            $calc_cpf = $count - $digitos_cpf;
            $str = substr($str, $calc_cpf, $count);
        }

        $cep = "#####-###";
        $dataBR = "##/##/####";
        $dataUS = "####-##-##";

        if ($formatacao == 'telefone') {
            if ($str{0} == 0 && $str{1} != 0) {
                $str = substr($str, 1);
            } elseif ($str{0} == 0 && $str{1} == 0) {
                $str = substr($str, 2);
            }
        }

        $telefone = "$str (erro masc)";
        if (strlen($str) == 10) {
            $telefone = "(##) ####-####";
        }
        if (strlen($str) == 11 || strlen($str) == 12) {
            $telefone = "(##) #####-####";
        }

        if ($personalizado) {
            $masc = $formatacao;
        } else {
            switch ($formatacao) {
                case 'cpf':
                    $masc = $cpf;
                    break;

                case 'cnpj':
                    $masc = $cnpj;
                    break;

                case 'cep':
                    $masc = $cep;
                    break;

                case 'dataBR':
                case 'data':
                    $masc = $dataBR;
                    $str = date("dmY", strtotime($str));
                    break;

                case 'dataUS':
                    $masc = $dataUS;
                    break;

                case 'telefone':
                    $masc = $telefone;
                    break;

                default:
                    $masc = 'erro';
                    break;
            }
        }

        // monta o retorno
        $marcara = '';
        if ($masc != null) {
            $k = 0;
            for ($i = 0; $i <= strlen($masc) - 1; $i++) {
                if ($masc[$i] == '#') {
                    if (isset($str[$k])) {
                        $marcara .= $str[$k++];
                    }
                } else {
                    if (isset($masc[$i])) {
                        $marcara .= $masc[$i];
                    }
                }
            }
        }
        return $marcara;
    }
}

/**
 * Função usada para visualizar melhor o array ou um objeto
 */
function printr($ao)
{
    echo '<pre>';
    print_r($ao);
    echo '</pre>';
}

