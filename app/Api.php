<?php

class Api extends VSM
{
    public function __construct()
    {
        $this->helper('util');
        $this->model(array('imovel'));
    }

    public function index()
    {
        $this->listar();
    }

    public function listar()
    {

        $imoveis_api = $this->imoveis_api();

        $this->data['imoveis'] = json_decode($imoveis_api, true);

        $this->template('api/listar', $this->data);
    }
}