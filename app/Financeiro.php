<?php

class Financeiro extends VSM
{
    public function __construct()
    {

        $this->helper('util');
        $this->model(array('financeiro'));
    }

    public function index()
    {
        $this->listar();
    }

    public function listar($id=null)
    {
        $get = $this->financeiro_model->listar($id);
        foreach ($get as $k => $v) {
        }
        $this->data['financeiro'] = $get;

        $this->template('financeiro/listar', $this->data);
    }

    public function salvar()
    {
        if ($_POST) {
            $dados = [
                'financeiro_mensalidade_ok' => $_POST['financeiro_mensalidade_ok'],
                'financeiro_repasse_ok' => $_POST['financeiro_repasse_ok'],
            ];

            $update = $this->financeiro_model->update($dados, ['financeiro_id'=>$_POST['financeiro_id']]);
            if (!$update) {
                set_msg_flash("Não conseguimos atualizar a parcela!", 'danger');
            } else {
                set_msg_flash("Parcela atualizada com sucesso!", 'success');
            }
        }

        redirect('financeiro/listar');

    }

    public function excluir($id=null)
    {
        $delete = $this->financeiro_model->delete(['financeiro_id' => $id]);

        if (!$delete) {
            set_msg_flash("Não conseguimos excluir a parcela!", 'danger');
        } else {
            set_msg_flash("Parcela excluída com sucesso!", 'success');
        }

        redirect('financeiro/listar');
    }
}