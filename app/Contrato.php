<?php

class Contrato extends VSM
{
    public function __construct()
    {

        // seto os campos como vazio
        $this->data['contrato_id']                  = '';
        $this->data['contrato_data_inicio']         = '';
        $this->data['contrato_data_fim']            = '';
        $this->data['contrato_taxa_admin']          = '';
        $this->data['contrato_valor_aluguel']       = '';
        $this->data['contrato_valor_condominio']    = '';
        $this->data['contrato_valor_iptu']          = '';

        $this->helper('util');
        $this->model(array('contrato', 'imovel', 'proprietario', 'cliente', 'financeiro'));
    }

    public function index()
    {
        $this->listar();
    }

    public function novo()
    {
        $this->data['acao'] = "Incluir";

        // pegando imoveis
        $get_imovel = $this->imovel_model->all();
        $this->data['imoveis'] = $get_imovel;
        foreach ($get_imovel as $k => $v) {
            $get_imovel[$k]->imovel_selected = '';
        }

        // pegando proprietários
        $get_prop = $this->proprietario_model->all();
        $this->data['proprietarios'] = $get_prop;
        foreach ($get_prop as $k => $v) {
            $get_prop[$k]->prop_selected = '';
        }

        // pegando clientes
        $get_cli = $this->cliente_model->all();
        $this->data['clientes'] = $get_cli;
        foreach ($get_cli as $k => $v) {
            $get_cli[$k]->cli_selected = '';
        }

        $this->template('contrato/incluir_editar', $this->data);
    }

    public function editar($id = null)
    {
        $this->data['acao'] = "Editar";

        $get = $this->contrato_model->get_by_campo(['contrato_id' => $id]);

        // pegando imoveis
        $get_imovel = $this->imovel_model->all();
        $this->data['imoveis'] = $get_imovel;
        foreach ($get_imovel as $k => $v) {
            $get_imovel[$k]->imovel_selected = '';
            if($get_imovel[$k]->imovel_id == $get['imovel_id']){
                $get_imovel[$k]->imovel_selected = 'selected';
            }
        }

        // pegando proprietários
        $get_prop = $this->proprietario_model->all();
        $this->data['proprietarios'] = $get_prop;
        foreach ($get_prop as $k => $v) {
            $get_prop[$k]->prop_selected = '';
            if($get_prop[$k]->proprietario_id == $get['proprietario_id']){
                $get_prop[$k]->prop_selected = 'selected';
            }
        }

        // pegando clientes
        $get_cli = $this->cliente_model->all();
        $this->data['clientes'] = $get_cli;
        foreach ($get_cli as $k => $v) {
            $get_cli[$k]->cli_selected = '';
            if($get_cli[$k]->cliente_id == $get['cliente_id']){
                $get_cli[$k]->cli_selected = 'selected';
            }
        }

        $this->data = array_merge($this->data, $get);

        $this->template('contrato/incluir_editar', $this->data);
    }

    public function listar()
    {
        $delete = $this->financeiro_model->delete(['contrato_id' => 12]);
        $get = $this->contrato_model->listar();
        foreach ($get as $k => $v) {
        }
        $this->data['contratos'] = $get;

        $this->template('contrato/listar', $this->data);
    }

    public function salvar()
    {
        if ($_POST) {

            $this->data['contrato_data_inicio']         = '';
            $this->data['contrato_data_fim']            = '';
            $this->data['contrato_taxa_admin']          = '';
            $this->data['contrato_valor_aluguel']       = '';
            $this->data['contrato_valor_condominio']    = '';
            $this->data['contrato_valor_iptu']          = '';
            $this->data['imovel_id']                    = '';
            $this->data['proprietario_id']              = '';
            $this->data['cliente_id']                   = '';

            $dados = [
                'contrato_data_inicio' => $_POST['contrato_data_inicio'],
//                'contrato_data_fim' => $_POST['contrato_data_fim'],
                'contrato_data_fim' => $_POST['contrato_data_inicio'],
                'contrato_taxa_admin' => limpa_valor($_POST['contrato_taxa_admin']),
                'contrato_valor_aluguel' => limpa_valor($_POST['contrato_valor_aluguel']),
                'contrato_valor_condominio' => limpa_valor($_POST['contrato_valor_condominio']),
                'contrato_valor_iptu' => limpa_valor($_POST['contrato_valor_iptu']),
                'imovel_id' => $_POST['imovel_id'],
                'proprietario_id' => $_POST['proprietario_id'],
                'cliente_id' => $_POST['cliente_id']
            ];

            if (empty($_POST['contrato_id'])) {
                // cadastrar

                $existe = $this->contrato_model->existe([
                        'imovel_id'=>$dados['imovel_id'],
                        'proprietario_id'=>$dados['proprietario_id'],
                        'cliente_id'=>$dados['cliente_id']
                ]);
                if(!$existe){
                    $insert = $this->contrato_model->insert($dados);
                    if (!$insert) {
                        set_msg_flash("Não conseguimos incluir o contrato!", 'danger');
                    } else {
                        $id = $insert;

                        set_msg_flash("Contrato inserido com sucesso!", 'success');
                    }
                }else{
                    set_msg_flash("Já tem um contrato para este imóvel e este cliente!", 'danger');
                    redirect('contrato/novo');
                }

            } else {
                // alterar

                $id = $_POST['contrato_id'];

                $get = $this->contrato_model->get_by_campo(['contrato_id'=>$id]);
                if($_POST['imovel_id'] != $get['imovel_id'] && $_POST['proprietario_id'] != $get['proprietario_id'] && $_POST['cliente_id'] != $get['cliente_id'])
                {
                    $existe = $this->contrato_model->existe([
                        'imovel_id'=>$_POST['imovel_id'],
                        'proprietario_id'=>$_POST['proprietario_id'],
                        'cliente_id'=>$_POST['cliente_id']
                    ]);
                    if($existe){
                        set_msg_flash("Já tem um contrato para este imóvel e este cliente!", 'danger');
                        redirect("contrato/editar/{$id}");
                    }
                }

                $update = $this->contrato_model->update($dados, ['contrato_id'=>$id]);
                if (!$update) {
                    set_msg_flash("Não conseguimos salvar o contrato!", 'danger');
                } else {
                    set_msg_flash("Contrato salvo com sucesso!", 'success');

                    // excluindo financeiro
                    $delete = $this->financeiro_model->delete(['contrato_id' => $id]);
                }
            }

            // cadastrar financeiro

            $data_termino = date('Y-m-t', strtotime('+12 month', strtotime($dados['contrato_data_inicio'])));
            $update_cont = $this->contrato_model->update(['contrato_data_fim'=>$data_termino], ['contrato_id'=>$id]);

            $arr_parcelas = gerar_parcelas(12, $_POST['contrato_data_inicio']);

            $i = 1;
            foreach ($arr_parcelas as $dt_venc) {

                if($i == 1){
                    $dia_inicio = date('d', strtotime($dt_venc));
                    if($dia_inicio == '01'){
                        $valor_aluguel = $dados['contrato_valor_aluguel'];
                    }else{

                        $data_fim = date('Y-m-t', strtotime($dt_venc));
                        $dif_data = strtotime($data_fim) - strtotime($dt_venc);
                        $qtd_dias = floor($dif_data / (60 * 60 * 24));
                        $diaria = ($dados['contrato_valor_aluguel'] / 30);
                        $valor_aluguel = ($diaria * $qtd_dias);

                    }

                    $vencimento = $dados['contrato_data_inicio'];

                }else{
                    $vencimento = date('Y', strtotime($dt_venc)).'-'.date('m', strtotime($dt_venc)).'-01';
                    $valor_aluguel = $dados['contrato_valor_aluguel'];
                }

                $mensalidade = ($valor_aluguel + $dados['contrato_taxa_admin'] + $dados['contrato_valor_condominio'] + $dados['contrato_valor_iptu']);
                $repasse = ($valor_aluguel + $dados['contrato_valor_iptu']);

                $dados_fin = [
                    'financeiro_vencimento'=>$vencimento,
                    'financeiro_mensalidade'=>$mensalidade,
                    'financeiro_repasse'=>$repasse,
                    'contrato_id'=>$id,
                ];
                $insert_fin = $this->financeiro_model->insert($dados_fin);

                $i++;
            }
        }

        if (isset($_POST['salvar_cont_ed'])) {
            redirect("contrato/editar/{$id}");
        } elseif (isset($_POST['salvar'])) {
            redirect('contrato/listar');
        } else {
            redirect('contrato/listar');
        }

    }

    public function excluir($id=null)
    {
        $delete = $this->contrato_model->delete(['contrato_id' => $id]);

        if (!$delete) {
            set_msg_flash("Não conseguimos excluir o contrato!", 'danger');
        } else {
            set_msg_flash("Contrato excluído com sucesso!", 'success');
        }

        redirect('contrato/listar');
    }
}