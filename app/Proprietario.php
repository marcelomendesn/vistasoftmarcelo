<?php

class Proprietario extends VSM
{
    public function __construct()
    {

        // seto os campos como vazio
        $this->data['proprietario_id'] = '';
        $this->data['proprietario_nome'] = '';
        $this->data['proprietario_email'] = '';
        $this->data['proprietario_telefone'] = '';
        $this->data['proprietario_data_repasse'] = '';

        $this->helper('util');
        $this->model(array('proprietario'));
    }

    public function index()
    {
        $this->listar();
    }

    public function novo()
    {
        $this->data['acao'] = "Incluir";
        $this->template('proprietario/incluir_editar');
    }

    public function editar($id = null)
    {
        if($id != null){
            $this->data['acao'] = "Editar";

            $get = $this->proprietario_model->get_by_campo(['proprietario_id' => $id]);
            $this->data = array_merge($this->data, $get);

            $this->template('proprietario/incluir_editar', $this->data);
        }else{
            redirect('proprietario/listar');
        }

    }

    public function listar()
    {
        $get = $this->proprietario_model->all();
        foreach ($get as $k => $v) {
            $get[$k]->proprietario_telefone = formatar($get[$k]->proprietario_telefone, 'telefone');
            $get[$k]->proprietario_data_repasse = formatar($get[$k]->proprietario_data_repasse, 'data');
        }
        $this->data['proprietarios'] = $get;

        $this->template('proprietario/listar', $this->data);
    }

    public function salvar()
    {
        if ($_POST) {
            $dados = [
                'proprietario_nome' => limpa_str($_POST['proprietario_nome']),
                'proprietario_email' => $_POST['proprietario_email'],
                'proprietario_telefone' => str_replace(' ', '',limpa_str($_POST['proprietario_telefone'])),
                'proprietario_data_repasse' => $_POST['proprietario_data_repasse'],
            ];

            if (empty($_POST['proprietario_id'])) {
                // cadastrar

                $existe = $this->proprietario_model->existe(['proprietario_email'=>$_POST['proprietario_email']]);

                if(!$existe){

                    $insert = $this->proprietario_model->insert($dados);
                    if (!$insert) {
                        set_msg_flash("Não conseguimos incluir proprietário!", 'danger');
                    } else {
                        $id = $insert;
                        set_msg_flash("Proprietário inserido com sucesso!", 'success');
                    }
                }else{
                    set_msg_flash("E-mail {$_POST['proprietario_email']} já está cadastrado!", 'danger');
                    redirect('proprietario/novo');
                }

            } else {
                // alterar

                $id = $_POST['proprietario_id'];

                $get = $this->proprietario_model->get_by_campo(['proprietario_id'=>$id]);
                if($_POST['proprietario_email'] != $get['proprietario_email'])
                {
                    $existe = $this->proprietario_model->existe(['proprietario_email'=>$_POST['proprietario_email']]);
                    if($existe){
                        set_msg_flash("E-mail {$_POST['proprietario_email']} já está cadastrado!", 'danger');
                        redirect("proprietario/editar/{$id}");
                    }
                }

                $update = $this->proprietario_model->update($dados, ['proprietario_id'=>$id]);
                if (!$update) {
                    set_msg_flash("Não conseguimos salvar proprietário!", 'danger');
                } else {
                    set_msg_flash("Proprietário salvo com sucesso!", 'success');
                }
            }
        }

        if (isset($_POST['salvar_cont_ed'])) {
            redirect("proprietario/editar/{$id}");
        } elseif (isset($_POST['salvar'])) {
            redirect('proprietario/listar');
        } else {
            redirect('proprietario/listar');
        }
    }

    public function excluir($id=null)
    {
        $delete = $this->proprietario_model->delete(['proprietario_id' => $id]);

        if (!$delete) {
            set_msg_flash("Não conseguimos excluir proprietário!", 'danger');
        } else {
            set_msg_flash("Proprietário excluído com sucesso!", 'success');
        }

        redirect('proprietario/listar');
    }
}