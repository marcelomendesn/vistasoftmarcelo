<?php

class FinanceiroModel extends \Database
{

    public function __construct()
    {
        parent::__construct();
        $this->table = "financeiro";
    }

    public function listar($id=null)
    {
        $sql = "SELECT * FROM {$this->table} f ";
        $sql .= "INNER JOIN contratos c ON ";
        $sql .= "f.contrato_id = c.contrato_id ";
        $sql .= "INNER JOIN clientes l ON ";
        $sql .= "c.cliente_id = l.cliente_id ";
        $sql .= "INNER JOIN proprietarios p ON ";
        $sql .= "c.proprietario_id = p.proprietario_id ";

        if($id != null){
            $sql .= "WHERE l.cliente_id = {$id}";
        }

        $pdo = $this::conexao();
        $listar = $pdo->prepare($sql);
        if($listar->execute()){
            return $listar->fetchAll(PDO::FETCH_OBJ);
        }else{
            return null;
        }
    }
}