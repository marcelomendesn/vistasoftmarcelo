<?php

class ContratoModel extends \Database
{

    public function __construct()
    {
        parent::__construct();
        $this->table = "contratos";
    }

    public function listar()
    {
        $sql = "SELECT * FROM {$this->table} c ";
        $sql .= "INNER JOIN imoveis i ON ";
        $sql .= "c.imovel_id = i.imovel_id ";
        $sql .= "INNER JOIN proprietarios p ON ";
        $sql .= "c.proprietario_id = p.proprietario_id ";
        $sql .= "INNER JOIN clientes l ON ";
        $sql .= "c.cliente_id = l.cliente_id";

        $pdo = $this::conexao();
        $listar = $pdo->prepare($sql);
        if($listar->execute()){
            return $listar->fetchAll(PDO::FETCH_OBJ);
        }else{
            return null;
        }
    }
}