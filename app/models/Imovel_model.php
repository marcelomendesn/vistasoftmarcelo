<?php

class ImovelModel extends \Database
{

    public function __construct()
    {
        parent::__construct();
        $this->table = "imoveis";
    }

    public function listar()
    {
        $sql = "SELECT * FROM {$this->table} i ";
        $sql .= "INNER JOIN proprietarios p ON ";
        $sql .= "i.proprietario_id = p.proprietario_id";

        $pdo = $this::conexao();
        $listar = $pdo->prepare($sql);
        if($listar->execute()){
            return $listar->fetchAll(PDO::FETCH_OBJ);
        }else{
            return null;
        }
    }
}