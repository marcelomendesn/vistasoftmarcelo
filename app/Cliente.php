<?php

class Cliente extends VSM
{
    public function __construct()
    {

        // seto os campos como vazio
        $this->data['cliente_id'] = '';
        $this->data['cliente_nome'] = '';
        $this->data['cliente_email'] = '';
        $this->data['cliente_telefone'] = '';

        $this->helper('util');
        $this->model(array('cliente'));
    }

    public function index()
    {
        $this->listar();
    }

    public function novo()
    {
        $this->data['acao'] = "Incluir";
        $this->template('cliente/incluir_editar');
    }

    public function editar($id = null)
    {
        $this->data['acao'] = "Editar";

        $get = $this->cliente_model->get_by_campo(['cliente_id' => $id]);
        $this->data = array_merge($this->data, $get);

        $this->template('cliente/incluir_editar', $this->data);
    }

    public function listar()
    {
        $get = $this->cliente_model->all();
        foreach ($get as $k => $v) {
            $get[$k]->cliente_telefone = formatar($get[$k]->cliente_telefone, 'telefone');
        }
        $this->data['clientes'] = $get;

        $this->template('cliente/listar', $this->data);
    }

    public function salvar()
    {
        if ($_POST) {
            $dados = [
                'cliente_nome' => limpa_str($_POST['cliente_nome']),
                'cliente_email' => $_POST['cliente_email'],
                'cliente_telefone' => str_replace(' ', '',limpa_str($_POST['cliente_telefone'])),
            ];

            if (empty($_POST['cliente_id'])) {
                // cadastrar

                $existe = $this->cliente_model->existe(['cliente_email'=>$_POST['cliente_email']]);
                if(!$existe){
                    $insert = $this->cliente_model->insert($dados);
                    if (!$insert) {
                        set_msg_flash("Não conseguimos incluir cliente!", 'danger');
                    } else {
                        $id = $insert;
                        set_msg_flash("Cliente inserido com sucesso!", 'success');
                    }
                }else{
                    set_msg_flash("E-mail {$_POST['cliente_email']} já está cadastrado!", 'danger');
                    redirect('cliente/novo');
                }

            } else {
                // alterar

                $id = $_POST['cliente_id'];

                $get = $this->cliente_model->get_by_campo(['cliente_id'=>$id]);
                if($_POST['cliente_email'] != $get['cliente_email'])
                {
                    $existe = $this->cliente_model->existe(['cliente_email'=>$_POST['cliente_email']]);
                    if($existe){
                        set_msg_flash("E-mail {$_POST['cliente_email']} já está cadastrado!", 'danger');
                        redirect("cliente/editar/{$id}");
                    }
                }

                $update = $this->cliente_model->update($dados, ['cliente_id'=>$id]);
                if (!$update) {
                    set_msg_flash("Não conseguimos salvar cliente!", 'danger');
                } else {
                    set_msg_flash("Cliente salvo com sucesso!", 'success');
                }
            }
        }

        if (isset($_POST['salvar_cont_ed'])) {
            redirect("cliente/editar/{$id}");
        } elseif (isset($_POST['salvar'])) {
            redirect('cliente/listar');
        } else {
            redirect('cliente/listar');
        }

    }

    public function excluir($id=null)
    {
        $delete = $this->cliente_model->delete(['cliente_id' => $id]);

        if (!$delete) {
            set_msg_flash("Não conseguimos excluir cliente!", 'danger');
        } else {
            set_msg_flash("Cliente excluído com sucesso!", 'success');
        }

        redirect('cliente/listar');
    }
}