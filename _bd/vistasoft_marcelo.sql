-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 15-Dez-2020 às 04:47
-- Versão do servidor: 5.7.24
-- versão do PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vistasoft_marcelo`
--
CREATE DATABASE IF NOT EXISTS `vistasoft_marcelo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `vistasoft_marcelo`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `cliente_id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_nome` varchar(100) NOT NULL,
  `cliente_email` varchar(100) NOT NULL,
  `cliente_telefone` varchar(11) NOT NULL,
  PRIMARY KEY (`cliente_id`),
  UNIQUE KEY `idx_cliente_email` (`cliente_email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`cliente_id`, `cliente_nome`, `cliente_email`, `cliente_telefone`) VALUES
(1, 'Marcelo M N', 'marcelo@teste.com', '48999584119'),
(2, 'Marcelo Mendes', 'marcelo@teste.com.br', '48999584119');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contratos`
--

DROP TABLE IF EXISTS `contratos`;
CREATE TABLE IF NOT EXISTS `contratos` (
  `contrato_id` int(11) NOT NULL AUTO_INCREMENT,
  `contrato_data_inicio` date NOT NULL,
  `contrato_data_fim` date NOT NULL,
  `contrato_taxa_admin` float(10,2) DEFAULT NULL,
  `contrato_valor_aluguel` float(10,2) DEFAULT NULL,
  `contrato_valor_condominio` float(10,2) DEFAULT NULL,
  `contrato_valor_iptu` float(10,2) DEFAULT NULL,
  `imovel_id` int(11) NOT NULL,
  `proprietario_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  PRIMARY KEY (`contrato_id`),
  KEY `fk_contratos_imoveis1_idx` (`imovel_id`),
  KEY `fk_contratos_proprietarios1_idx` (`proprietario_id`),
  KEY `fk_contratos_clientes1_idx` (`cliente_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `financeiro`
--

DROP TABLE IF EXISTS `financeiro`;
CREATE TABLE IF NOT EXISTS `financeiro` (
  `financeiro_id` int(11) NOT NULL AUTO_INCREMENT,
  `financeiro_vencimento` date NOT NULL COMMENT 'Data de vencimento',
  `financeiro_mensalidade` float(10,2) NOT NULL COMMENT 'Valor da mensalidade',
  `financeiro_mensalidade_ok` enum('0','1') NOT NULL DEFAULT '0',
  `financeiro_repasse` float(10,2) NOT NULL COMMENT 'Valor do repasse',
  `financeiro_repasse_ok` enum('0','1') NOT NULL DEFAULT '0',
  `contrato_id` int(11) NOT NULL,
  PRIMARY KEY (`financeiro_id`),
  KEY `fk_financeiro_contratos1_idx` (`contrato_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `imoveis`
--

DROP TABLE IF EXISTS `imoveis`;
CREATE TABLE IF NOT EXISTS `imoveis` (
  `imovel_id` int(11) NOT NULL AUTO_INCREMENT,
  `imovel_endereco` varchar(100) NOT NULL,
  `imovel_cod_vista` varchar(70) DEFAULT NULL,
  `proprietario_id` int(11) NOT NULL,
  PRIMARY KEY (`imovel_id`),
  KEY `fk_imoveis_proprietarios1_idx` (`proprietario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `proprietarios`
--

DROP TABLE IF EXISTS `proprietarios`;
CREATE TABLE IF NOT EXISTS `proprietarios` (
  `proprietario_id` int(11) NOT NULL AUTO_INCREMENT,
  `proprietario_nome` varchar(100) NOT NULL,
  `proprietario_email` varchar(100) NOT NULL,
  `proprietario_telefone` varchar(100) NOT NULL,
  `proprietario_data_repasse` date NOT NULL,
  PRIMARY KEY (`proprietario_id`),
  UNIQUE KEY `idx_email_proprietario` (`proprietario_email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `proprietarios`
--

INSERT INTO `proprietarios` (`proprietario_id`, `proprietario_nome`, `proprietario_email`, `proprietario_telefone`, `proprietario_data_repasse`) VALUES
(1, 'Marcelo Mendes', 'marcelo@teste.com', '48999584118', '2020-12-17'),
(4, 'Marcelo', 'marcelo@teste.com.br', '4899584119', '2020-12-31');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `contratos`
--
ALTER TABLE `contratos`
  ADD CONSTRAINT `fk_contratos_clientes1` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`cliente_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contratos_imoveis1` FOREIGN KEY (`imovel_id`) REFERENCES `imoveis` (`imovel_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contratos_proprietarios1` FOREIGN KEY (`proprietario_id`) REFERENCES `proprietarios` (`proprietario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `financeiro`
--
ALTER TABLE `financeiro`
  ADD CONSTRAINT `fk_financeiro_contratos1` FOREIGN KEY (`contrato_id`) REFERENCES `contratos` (`contrato_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `imoveis`
--
ALTER TABLE `imoveis`
  ADD CONSTRAINT `fk_imoveis_proprietarios1` FOREIGN KEY (`proprietario_id`) REFERENCES `proprietarios` (`proprietario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
